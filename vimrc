set nocompatible

source ~/.vim/bundles.vim
source ~/.vim/functions.vim
source ~/.vim/keybindings.vim

for f in split(glob('~/.vim/settings/*.vim'), '\n')
    exe 'source' f
endfor

source ~/.vim/global.vim
source ~/.vim/auto_commands.vim
source ~/.vim/syntax.vim

" my stuff
set guioptions+=m  "remove menu bar
set guioptions-=T  "remove toolbar

map <C-s> :<cr>

set hidden                        " Handle multiple buffers better.

set wildmenu                      " Enhanced command line completion.
set wildmode=list:longest         " Complete files like a shell.

set ignorecase                    " Case-insensitive searching.
set smartcase                     " But case-sensitive if expression contains a capital letter.

set number                        " Show line numbers.
set norelativenumber              " no relative line number
set ruler                         " Show cursor position.
set cursorline                    " Highlight current line
set incsearch                     " Highlight matches as you type.
set hlsearch                      " Highlight matches.

set wrap                          " Turn on line wrapping.
set scrolloff=3                   " Show 3 lines of context around the cursor.

set title                         " Set the terminal's title

set visualbell                    " No beeping.

set nobackup                      " Don't make a backup before overwriting a file.
set nowritebackup                 " And again.
set directory=$HOME/.vim/tmp//,.  " Keep swap files in one location

" UNCOMMENT TO USE
set tabstop=2                    " Global tab width.
set shiftwidth=2                 " And again, related.
set expandtab                    " Use spaces instead of tabs
" Tab mappings.
map <leader>tt :tabnew<cr>
map <leader>te :tabedit
map <leader>tc :tabclose<cr>
map <leader>to :tabonly<cr>
map <leader>tn :tabnext<cr>
map <leader>tp :tabprevious<cr>
map <leader>tf :tabfirst<cr>
map <leader>tl :tablast<cr>
map <leader>tm :tabmove

map <C-s> :w<cr>
map ,h :nohl<cr>
map ,t :! npm run protractor %<cr>
map ,a :! git add %<cr>
map ,c :Gcommit<cr>
map ,p :! git push origin develop<cr>
map ,f :! git pull origin develop<cr>

nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l
"

map <C-n> :NERDTreeToggle<CR>
let NERDTreeWinSize=26
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

autocmd BufNewFile,BufRead *_spec.rb compiler rspec

" things stolen from Gary Berndhart .vimrc
function! RenameFile()
  let old_name = expand('%')
  let new_name = input('Le nouveau nom: ', expand('%'), 'file')
  if new_name != '' && new_name != old_name
    exec ':saveas ' . new_name
    exec ':silent !rm ' . old_name
    redraw!
  endif
endfunction

map <leader>n :call RenameFile()<cr>

function! AutoCompleteTab()
  let col = col(".") - 1
  if !col || getline(".")[col - 1] !~ '\k'
    return "\<tab>"
  else
    return "\<C-n>"
  endif
endfunction

inoremap <tab> <c-r>=AutoCompleteTab<cr>

iabbrev dp do
iabbrev tp to
iabbrev EMber Ember
iabbrev fn function
iabbrev retunr return
iabbrev retun return
iabbrev exp expect
iabbrev shoudl should
iabbrev sould should
iabbrev Save save
iabbrev calss class
iabbrev clss class

" Ctrl+P
let g:ctrlp_match_window_bottom = 1
set runtimepath^=~/.vim/bundle/ctrlp.vim


" Moving line
nnoremap <C-S-j> :m .+1<CR>==
nnoremap <C-S-k> :m .-2<CR>==

" Guifont
set guifont=DejaVu\ Sans\ Mono

" ColorScheme
colorscheme badwolf

" CtrlS
noremap <C-S> :update<CR>
vnoremap <C-S> <C-C>:update<CR>
inoremap <C-S> <C-O>:update<CR>

" html autocompletion
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags

" using vim addons manager
set nocompatible | filetype indent plugin on | syn on

"javascript-libraries-syntax
let g:used_javascript_libs = 'jquery, angularjs, angularui, react, requirejs, jasmine'

" UltiSnips conf
" Trigger configuration. Do not use <tab>
" if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-a>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" omni completion
filetype plugin on
set omnifunc=syntaxcomplete#Complete

" vim syntax on
syntax on
filetype off
filetype on

" Using enter to select autocomplete option
:inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Enhancing autocompletion
:set completeopt=longest,menuone
inoremap <expr> <C-n> pumvisible() ? '<C-n>' :
  \ '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

inoremap <expr> <M-,> pumvisible() ? '<C-n>' :
  \ '<C-x><C-o><C-n><C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

" required by vim-less
nnoremap <Leader>m :w <BAR> !lessc % > %:t:r.css<CR><space>

set t_Co=256   " This is may or may not needed.

set background=light
colorscheme PaperColor "
