filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'NLKNguyen/papercolor-theme'

Bundle "https://github.com/gmarik/vundle"

" System:
Bundle "https://github.com/clones/vim-l9.git"

" Ultimate auto-completion system for Vim
Bundle "https://github.com/ervandew/supertab.git"

" Completion cache
Bundle 'https://github.com/Shougo/neocomplcache'

" Toggle showing/hiding both the quickfix list and the location list
Bundle "https://github.com/milkypostman/vim-togglelist.git"

" A set of Vim Script utilities (Required for quickfix signs)
Bundle "https://github.com/tomtom/tlib_vim.git"

" Show marks in the gutter for various items (marks, quickfix, location, errors)
Bundle "https://github.com/tomtom/quickfixsigns_vim.git"

" Pairs of Mappings in Vim
Bundle "https://github.com/tpope/vim-unimpaired.git"

" Adds status bar to Vim
Bundle "bling/vim-airline"

Bundle "https://github.com/vim-scripts/HTML-AutoCloseTag"

" Working With Colors:
" Show color previews and help pick the one you want
Bundle "https://github.com/Rykka/colorv.vim.git"

" Vim plugin that displays tags in a window, ordered by class etc
Bundle "https://github.com/majutsushi/tagbar.git"

" Vim plugin for the Perl module / CLI script 'ack'
Bundle "https://github.com/mileszs/ack.vim.git"

" File Manipulation:
" Graph your Vim undo tree in style.
Bundle "https://github.com/sjl/gundo.vim.git"

" Support for user-defined text objects
Bundle "https://github.com/kana/vim-textobj-user.git"

" Support text objects bound to indention level
Bundle "https://github.com/austintaylor/vim-indentobject.git"

" Vim script for text filtering and alignment
Bundle "https://github.com/godlygeek/tabular.git"

" Vim plugin, provides insert mode auto-completion for quotes, parens, brackets, etc.
Bundle "https://github.com/Townk/vim-autoclose.git"

" quoting/parenthesizing made simple
Bundle "https://github.com/tpope/vim-surround.git"

" https:
" webapi access for vim
Bundle "https://github.com/mattn/webapi-vim.git"

" vimscript for gist
Bundle "https://github.com/mattn/gist-vim.git"

" a Git wrapper so awesome, it should be illegal
Bundle "https://github.com/tpope/vim-fugitive.git"

" Programming:
" General:
" Syntax checking hacks for vim
Bundle "https://github.com/scrooloose/syntastic.git"

" Color coding of pairs of parenthesis, braces and brackets
Bundle "https://github.com/kien/rainbow_parentheses.vim.git"

" Comment Toggling
Bundle "https://github.com/scrooloose/nerdcommenter.git"

" Team:
" Colaborative editing
Bundle "https://github.com/FredKSchott/CoVim.git"

" Markdown:
" Vim Markdown runtime files
Bundle "https://github.com/tpope/vim-markdown.git"

" Javascript:
" Vastly improved vim's javascript indentation.
Bundle "https://github.com/pangloss/vim-javascript.git"

" Javascript libraries syntax
Bundle "https://github.com/othree/javascript-libraries-syntax.vim.git"

" jamsine snippets library
Bundle "https://github.com/claco/jasmine.vim.git"

" UltiSnips
Plugin 'SirVer/ultisnips'

" Snippets for angularJs
Bundle "https://github.com/matthewsimo/angular-vim-snippets.git"

" Snippets for various languages
Bundle "https://github.com/honza/vim-snippets.git"

" JSHint fork of jslint.vim
"Bundle "https://github.com/wookiehangover/jshint.vim.git"
Bundle "https://github.com/Shutnik/jshint2.vim"

" CoffeeScript support for vim
Bundle "https://github.com/kchmck/vim-coffee-script.git"

" JSON Highlighting for Vim
Bundle "https://github.com/leshill/vim-json.git"

" Python:
" jedi auto complete
Bundle "https://github.com/davidhalter/jedi-vim.git"

" HTML And XML:
" zen-coding for vim
Bundle "https://github.com/mattn/emmet-vim.git"
" MatchParen for HTML tags
Bundle "https://github.com/gregsexton/MatchTag.git"

" Provides sgml (xml, html, etc.) end tag completion
Bundle "https://github.com/ervandew/sgmlendtag.git"

" Templating:
" Vim plugin for Handlebars
Bundle "https://github.com/nono/vim-handlebars.git"

" CSS:
" Vim runtime files for Haml, Sass, and SCSS
Bundle "https://github.com/tpope/vim-haml.git"

" Add extra syntax highlighting for SCSS files
Bundle "https://github.com/cakebaker/scss-syntax.vim.git"

" Nginx:
" Syntax highlighting for nginx configuration files
Bundle "https://github.com/mutewinter/nginx.vim.git"

" Tmux:
" Syntax highlighting for tmux configuration files
Bundle "https://github.com/acustodioo/vim-tmux.git"

" Themes And Appearance:
" Colorsheme Scroller, Chooser, and Browser
Bundle "https://github.com/vim-scripts/ScrollColors.git"

" A colorful, dark color scheme for Vim.
Bundle "https://github.com/nanotech/jellybeans.vim.git"

" A Vim color scheme with dark and light variants
Bundle "https://github.com/noahfrederick/Hemisu.git"

" Precision colorscheme for the vim text editor
Bundle "https://github.com/altercation/vim-colors-solarized.git"

" More colorschemes
Bundle "https://github.com/daylerees/colour-schemes", { "rtp": "vim-themes/" }

" NERDTree
Bundle 'https://github.com/scrooloose/nerdtree.git'

" VimTODO
Bundle 'https://github.com/mivok/vimtodo.git'

" Ruby SnipMate
Bundle 'https://github.com/kaichen/vim-snipmate-ruby-snippets.git'
" Ack for vim
" Elixir
Bundle 'elixir-lang/vim-elixir'
Bundle 'https://github.com/chrisbra/csv.vim.git'

" All of your Bundles must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" CtrlP
Bundle "https://github.com/kien/ctrlp.vim"

" CtrlS
:inoremap <c-s> <Esc>:Update<CR>

" Vim-less
Plugin 'groenewege/vim-less'
